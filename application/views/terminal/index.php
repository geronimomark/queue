<?php $this->load->view("template/header_terminal");?>
<header>
    <div class="container">
        <div class="four columns" id="logo">
            <img src="<?php echo base_url()?>assets/css/terminal/1logo.png" class="scale-with-grid" alt="Logo"></div>
        <!-- Your logo-->

<!--        <div class="btn-responsive-menu"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
        <div class="twelve columns" id="top-nav">
            <ul>
                <li><a href="">Menu</a></li>
            </ul>
        </div>-->
    </div>
</header>
<!-- End Header-->

<section id="form_area_survey">
    <div id="shadow"></div>
    <article class="container">

        <div class="five columns">
            <form id="custom" action="" method="POST">

                <fieldset title="Terminal">
                    <legend>&nbsp;</legend>
                    <!--<h3 class="question">1. How do you describe your overall satisfaction or dissatisfaction here?</h3>-->
                    <h3 class="question">Terminal ID: <?php echo $this->session->userdata("terminal_id");?></h3>
                    <h3 class="question">Now Serving: <i id="customer_number"><?php if(isset($data[0]["cq_cust_number"])) echo $data[0]["cq_cust_number"];?></i></h3>
                    <h3 class="question">Total Customers: <i id="served"><?php if(isset($data[0]["cq_cust_total"])) echo $data[0]["cq_cust_total"];?></i></h3>
                </fieldset>
                <!-- End Step one -->
                <img style="cursor:pointer" id="refresh" src="<?php echo base_url();?>assets/images/refresh.png" width="35" height="35"/>
                <input id="next" type="submit" class="finish" value="Next Customer" />
            </form>
        </div>
    </article>
    <div id="shadow_2"></div>
</section>
<!-- End Form Area -->

<!--<table>
    <tr>
        <td>Terminal ID:</td>
        <td><?php //echo $this->session->userdata("terminal_id");?></td>
    </tr>
    <tr>
        <td>Now Serving:</td>
        <td id="customer_number"><?php if(isset($data[0]["cq_cust_number"])) echo $data[0]["cq_cust_number"];?></td>
    </tr>
    <tr>
        <td>Total Customers:</td>
        <td id="served"><?php if(isset($data[0]["cq_cust_total"])) echo $data[0]["cq_cust_total"];?></td>
    </tr>
    <tr>
        <td colspan='2'><button id="next">NEXT CUSTOMER</button></td>
    </tr>
</table>-->
<script>
    $(function() {
        var $next = $("#next"),
        $customer_number = $("#customer_number"),
        $served = $("#served"),
        $cnum = 0;
        $next.click(function(){
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url()?>main/next_customer",
                success: function(msg){
                    $cnum = msg.split("_");
                }
            });
            $customer_number.html($cnum[0]);
            $served.html($cnum[1]);
        });
        
        $("#refresh").click(function(){
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url()?>main/recall/" + $customer_number.text()
            });
        });
        
    });
</script>
<?php $this->load->view("template/footer");?>