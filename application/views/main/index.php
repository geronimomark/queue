<?php $this->load->view("template/header_main");?>
<!-- <div id="slider-code">
    <a class="buttons prev" href="#">left</a>
    <div class="viewport">
        <ul class="overview">
            <li><img src="<?php echo base_url()?>assets/images/1.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/2.png" /></li>
            <li><img src="<?php echo base_url()?>assets/images/3.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/4.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/5.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/6.jpg" /></li>  
            <li><img src="<?php echo base_url()?>assets/images/7.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/8.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/9.jpg" /></li>
            <li><img src="<?php echo base_url()?>assets/images/10.jpg" /></li>              
        </ul>
    </div>
    <a class="buttons next" href="#">right</a>
</div> -->

<!--<div class="container">
   <div id="content-slider">
      <div id="slider">   Slider container 
         <div id="mask">   Mask 

         <ul>
         <li id="first" class="firstanimation">   ID for tooltip and class for animation 
         <a href="#"> <img src="<?php echo base_url();?>assets/images/1.jpg" alt="Cougar"/> </a>
         <div class="tooltip"> <h1>Cougar</h1> </div>
         </li>
 
         <li id="second" class="secondanimation">
         <a href="#"> <img src="<?php echo base_url();?>assets/images/3.jpg" alt="Lions"/> </a>
         <div class="tooltip"> <h1>Lions</h1> </div>
         </li>

         <li id="third" class="thirdanimation">
         <a href="#"> <img src="<?php echo base_url();?>assets/images/4.jpg" alt="Snowalker"/> </a>
         <div class="tooltip"> <h1>Snowalker</h1> </div>
         </li>

         <li id="fourth" class="fourthanimation">
         <a href="#"> <img src="<?php echo base_url();?>assets/images/5.jpg" alt="Howling"/> </a>
         <div class="tooltip"> <h1>Howling</h1> </div>
         </li>

         <li id="fifth" class="fifthanimation">
         <a href="#"> <img src="<?php echo base_url();?>assets/images/6.jpg" alt="Sunbathing"/> </a>
         <div class="tooltip"> <h1>Sunbathing</h1> </div>
         </li>
         </ul>

         </div>   End Mask 
         <div class="progress-bar"></div>   Progress Bar 
      </div>   End Slider Container 
   </div>
</div>-->

<div id="shadow_top">
    <div class="container">
        <br />
        <div id="Div1" class="hasCountdown"></div>
    </div>
</div>

<?php if(isset($images)):?>
  <?php echo $images["styles"];?>

  <br /><br />
  <div class=carousel>
    <ul class=panes>
      <?php echo $images["images"];?>
    </ul>
  </div>
<?php endif;?>

<style>
    /*#slider-code { height: 125px; overflow:hidden; }
    #slider-code .viewport { float: left; width: 240px; height: 125px; overflow: hidden; position: relative; }
    #slider-code .buttons { display: block; margin: 30px 10px 0 0; float: left; }
    #slider-code .next { margin: 30px 0 0 10px;  }
    #slider-code .disable { visibility: hidden; }
    #slider-code .overview { list-style: none; position: absolute; padding: 0; margin: 0; left: 0; top: 0; }
    #slider-code .overview li{ float: left; margin: 0 20px 0 0; padding: 1px; height: 121px; border: 1px solid #dcdcdc; width: 236px;}                  */

/*#slider {
   background: #000;
   background: url(assets/images/ussc.jpg);
   border: 5px solid #eaeaea;
   box-shadow: 1px 1px 5px rgba(0,0,0,0.7);
   height: 90px;
   width: 200px;
   margin: 40px auto 0;
   overflow: visible;
   position: relative;
}
#mask {
   overflow: hidden;
   height: 320px; 
}
#slider ul {
   margin: 0;
   padding: 0;
   position: relative;
}

#slider li { 
   width: 680px;   Width Image 
   height: 320px;  Height Image 
   position: absolute;
   top: -325px;    Original Position - Outside of the Slider 
   list-style: none;
}
#slider li.firstanimation {
   animation: cycle 25s linear infinite;     
}

#slider li.secondanimation {
   animation: cycletwo 25s linear infinite;  
}

#slider li.thirdanimation {
   animation: cyclethree 25s linear infinite;      
}

#slider li.fourthanimation {
   animation: cyclefour 25s linear infinite;    
}

#slider li.fifthanimation {
   animation: cyclefive 25s linear infinite;    
}

 ANIMATION 

@keyframes cycle {
   0%  { top: 0px; }  When you start the slide, the first image is already visible 
   4%  { top: 0px; }  Original Position 
   16% { top: 0px; opacity:1; z-index:0; }  From 4% to 16 % = for 3 seconds the image is visible 
   20% { top: 325px; opacity: 0; z-index: 0; }  From 16% to 20% = for 1 second exit image 
   21% { top: -325px; opacity: 0; z-index: -1; }  Return to Original Position 
   92% { top: -325px; opacity: 0; z-index: 0; }
   96% { top: -325px; opacity: 0; }  From 96% to 100% = for 1 second enter image
   100%{ top: 0px; opacity: 1; }
}

@keyframes cycletwo {
   0%  { top: -325px; opacity: 0; }  Original Position 
   16% { top: -325px; opacity: 0; } Starts moving after 16% to this position 
   20% { top: 0px; opacity: 1; }
   24% { top: 0px; opacity: 1; }   From 20% to 24% = for 1 second enter image
   36% { top: 0px; opacity: 1; z-index: 0; }    From 24% to 36 % = for 3 seconds the image is visible 
   40% { top: 325px; opacity: 0; z-index: 0; }  From 36% to 40% = for 1 second exit image 
   41% { top: -325px; opacity: 0; z-index: -1; }    Return to Original Position 
   100%{ top: -325px; opacity: 0; z-index: -1; }
}

@keyframes cyclethree {
   0%  { top: -325px; opacity: 0; }
   36% { top: -325px; opacity: 0; }
   40% { top: 0px; opacity: 1; }
   44% { top: 0px; opacity: 1; } 
   56% { top: 0px; opacity: 1; } 
   60% { top: 325px; opacity: 0; z-index: 0; }
   61% { top: -325px; opacity: 0; z-index: -1; } 
   100%{ top: -325px; opacity: 0; z-index: -1; }
}

@keyframes cyclefour {
   0%  { top: -325px; opacity: 0; }
   56% { top: -325px; opacity: 0; }
   60% { top: 0px; opacity: 1; }
   64% { top: 0px; opacity: 1; }
   76% { top: 0px; opacity: 1; z-index: 0; }
   80% { top: 325px; opacity: 0; z-index: 0; }
   81% { top: -325px; opacity: 0; z-index: -1; }
   100%{ top: -325px; opacity: 0; z-index: -1; }
}
@keyframes cyclefive {
   0%  { top: -325px; opacity: 0; }
   76% { top: -325px; opacity: 0; }
   80% { top: 0px; opacity: 1; }
   84% { top: 0px; opacity: 1; }
   96% { top: 0px; opacity: 1; z-index: 0; }
   100%{ top: 325px; opacity: 0; z-index: 0; }
}

.progress-bar { 
   position: relative;
   top: -5px;
   width: 680px; 
   height: 5px;
   background: #000;
   animation: fullexpand 25s ease-out infinite;
}

 ANIMATION BAR 

@keyframes fullexpand {
    In these keyframes, the progress-bar is stationary 
   0%, 20%, 40%, 60%, 80%, 100% { width: 0%; opacity: 0; }

    In these keyframes, the progress-bar starts to come alive 
   4%, 24%, 44%, 64%, 84% { width: 0%; opacity: 0.3; }

    In these keyframes, the progress-bar moves forward for 3 seconds 
   16%, 36%, 56%, 76%, 96% { width: 100%; opacity: 0.7; }

    In these keyframes, the progress-bar has finished his path 
   17%, 37%, 57%, 77%, 97% { width: 100%; opacity: 0.3; }

    In these keyframes, the progress-bar will disappear and then resume the cycle 
   18%, 38%, 58%, 78%, 98% { width: 100%; opacity: 0; }
}

#slider .tooltip {
   background: rgba(0,0,0,0.7);
   width: 300px;
   height: 60px;
   position: relative;
   bottom: 75px;
   left: -320px;
}

#slider .tooltip h1 {
   color: #fff;
   font-size: 24px;
   font-weight: 300;
   line-height: 60px;
   padding: 0 0 0 10px;
}

#slider .tooltip {
   transition: all 0.3s ease-in-out; 
}

#slider li#first: hover .tooltip, 
#slider li#second: hover .tooltip, 
#slider li#third: hover .tooltip, 
#slider li#fourth: hover .tooltip, 
#slider li#fifth: hover .tooltip {
   left: 0px;
}

#slider: hover li, 
#slider: hover .progress-bar {
   animation-play-state: paused;
}*/

</style>
    <script>
        $(function() {
            
            var $div_counter = $("#Div1"),
            $html = "",
            $sound = 0,
            $idle = 1,
            $timer = setTimeout(function(){idle_media_display();},30000),
//            $media = "<table><tr><td><div id='f1_container'><div id='f1_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/2go.gif' width='250' height='250'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/bayad_center.jpg' width='250' height='250'/></div></div></div></td><td><div id='f2_container'><div id='f2_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/cash_card.jpg' width='250' height='250'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/cebu.png' width='250' height='250'/></div></div></div></td></tr><tr><td><div id='f3_container'><div id='f3_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/Tickets1.jpg' width='250' height='250'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/western_union.jpg' width='250' height='250'/></div></div></div></td><td><div id='f4_container'><div id='f4_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/eload.jpg' width='250' height='250'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/pal.jpg' width='250' height='250'/></div></div></div></td></tr></table>";
            $media = "<br /><br /><table><tr><td><div id='f1_container'><div id='f1_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/2go.jpg'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/2.png' /></div></div></div></td><td><div id='f2_container'><div id='f2_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/3.jpg' /></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/7.jpg' /></div></div></div></td><td rowspan='3' id='f7'><div id='f7_container'><div id='f7_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/cash_card.jpg' width='200px' height='310px'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/bayad_center.jpg' width='200px' height='310px'/></div></div></div></td></tr><tr><td><div id='f4_container'><div id='f4_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/4.jpg'/></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/8.jpg' /></div></div></div></td><td><div id='f5_container'><div id='f5_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/6.jpg' /></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/10.jpg' /></div></div></div></td></tr><tr><td><div id='f3_container'><div id='f3_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/5.jpg' /></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/9.jpg' /></div></div></div></td><td><div id='f6_container'><div id='f6_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/12.jpg' /></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/11.jpg' /></div></div></div></td></tr><tr><td align='center' colspan='3'><img src='<?php echo base_url();?>assets/images/mainimage.jpg' width='840' height='350'/></td></tr></table>";
            $new2 = new Array(),
            $idlelogo = setInterval(function(){add();},5000);

            clearInterval($idlelogo);
            num1("welcome");
            get_table();
            
            function get_table(){
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "<?php echo base_url()?>main/generate_table",
                    success: function(msg){
                        if(msg !== $html){
                            $html = msg;
                            $sound = 1;
                            $idle = 1;
                        }
                    }
                });
                if($sound === 1){
                    clearTimeout($timer);
                    clearInterval($idlelogo);
                    $div_counter.html($html);
                    $sound = 0;
                    $timer = setTimeout(function(){idle_media_display();},60000);
                }
            }
            
            function idle_media_display(){
                if($idle == 1){
                    $div_counter.html($media);
                    $idlelogo = setInterval(function(){add();},5000);
                    $idle = 0;
                }
            }
            
            function get_latest(){
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "<?php echo base_url()?>main/big_display",
                    success: function(msg){
                        if(msg != "error"){
                            $new2.push(msg);
                            speak();    
                        }
                    }
                });
            }
            
            function speak(){
                var arr = $new2[0].split("_");
                $new2.shift();
                $("#" + arr[0].toString()).addClass("blinkz");
                num1(arr[0].toString() + "_" + arr[1].toString(), "#" + arr[0].toString());
            }
            
            function num1(num, blink){
                $.ajax({
                    url:'<?php echo base_url();?>assets/audio/' + num + '.mp3',
                    type:'HEAD',
                    error: function()
                    {
                        var _1 = new Audio('<?php echo base_url();?>assets/audio/doorbell2.wav'); 
                        _1.play(); 
                    },
                    success: function()
                    {
                        var _1 = new Audio('<?php echo base_url();?>assets/audio/' + num + '.mp3'); 
                        _1.play(); 
                    }
                });
                if(blink)
                    setTimeout(function(){$(blink).removeClass("blinkz");}, 4000);
            }
            
            function add(){
                $("#f1_card").addClass("rotate");
                $("#f2_card").addClass("rotateX");
                $("#f3_card").addClass("rotate");
                $("#f4_card").addClass("rotateX");
                $("#f5_card").addClass("rotate");
                $("#f6_card").addClass("rotateX");
                $("#f7_card").addClass("rotate");
                setTimeout(function(){removeclass();},2500);
            }

            function removeclass(){
                $("#f1_card").removeClass("rotate");
                $("#f2_card").removeClass("rotateX");
                $("#f3_card").removeClass("rotate");
                $("#f4_card").removeClass("rotateX");
                $("#f5_card").removeClass("rotate");
                $("#f6_card").removeClass("rotateX");
                $("#f7_card").removeClass("rotate");
            }
             
            setInterval(function(){get_table();},2000);

            setInterval(function(){get_latest();},4000);
            
            // $('#slider-code').tinycarousel({ interval: true  });
            
        });
    </script>
<?php $this->load->view("template/footer");?>