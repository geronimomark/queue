<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
    <head>

        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Terminal App | USSC</title>
        <meta name="author" content="">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/terminal/base.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/terminal/skeleton.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/terminal/forms.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/terminal/layout.css">

            <!--<link rel="icon" href="<?php echo base_url();?>assets/images/logo.png" type="image/x-icon" />-->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" />

        <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
    </head>
    <body>