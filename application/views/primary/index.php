<?php $this->load->view("template/header_terminal");?>
<header>
    <div class="container">
        <div class="four columns" id="logo">
            <img src="<?php echo base_url()?>assets/css/terminal/1logo.png" class="scale-with-grid" alt="Logo"></div>
        <!-- Your logo-->

        <div class="btn-responsive-menu"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
        <div class="twelve columns" id="top-nav">
            <ul>
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>main/index">Main Display</a></li>
                <li><a id="a" href="">Terminal Mode</a></li>
                <li><a href="<?php echo base_url()?>main/settings">Queue Settings</a></li>
                <li><a href="<?php echo base_url()?>main/settings_2">Terminal Settings</a></li>
                <li><a href="<?php echo base_url()?>upload">Ads Settings</a></li>
            </ul>
        </div>
    </div>
</header>
<!-- End Header-->
<section id="form_area_survey">
    <div id="shadow"></div>
    <article class="container">

        <!--<div class="five columns">-->
<!--            <input id="b4" type="submit" class="finish" value="Terminal Settings" />
            <input id="b3" type="submit" class="finish" value="Queue Settings" />
            <input id="b2" type="submit" class="finish" value="Terminal Mode" />            
            <input id="b1" type="submit" class="finish" value="Main Display" />-->
        <!--</div>-->
        
        <table width="980px" align="center"><tr><td><div id='f1_container'><div id='f1_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/2go.jpg'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/2.png' /></div></div></div></td><td><div id='f2_container'><div id='f2_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/3.jpg' /></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/7.jpg' /></div></div></div></td><td rowspan='3' id='f7'><div id='f7_container'><div id='f7_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/cash_card.jpg' width='200px' height='310px'/></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/bayad_center.jpg' width='200px' height='310px'/></div></div></div></td></tr><tr><td><div id='f4_container'><div id='f4_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/4.jpg'/></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/8.jpg' /></div></div></div></td><td><div id='f5_container'><div id='f5_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/6.jpg' /></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/10.jpg' /></div></div></div></td></tr><tr><td><div id='f3_container'><div id='f3_card' class='shadow'><div class='front face'><img src='<?php echo base_url();?>assets/images/5.jpg' /></div><div class='test back face center'><img src='<?php echo base_url();?>assets/images/9.jpg' /></div></div></div></td><td><div id='f6_container'><div id='f6_card' class='shadow'><div class='front face2'><img src='<?php echo base_url();?>assets/images/12.jpg' /></div><div class='test back2 face2 center'><img src='<?php echo base_url();?>assets/images/11.jpg' /></div></div></div></td></tr></table>
    </article>
    <div id="shadow_2"></div>
</section>
<!-- End Form Area -->

<script>
    $(function() {
//        $("#b1").click(function(){
//            window.location.href = "<?php echo base_url()?>main/index";
//        });
//        $("#b2").click(function(){
//            newwindow=window.open("<?php echo base_url()?>main/terminal_mode","name","scrollbars=0,height=400,width=330");
//            if (window.focus) newwindow.focus(); 
//        });
        $("#a").click(function(e){
            e.preventDefault();
            newwindow=window.open("<?php echo base_url()?>main/terminal_mode","name","scrollbars=0,height=400,width=330");
            if (window.focus) newwindow.focus(); 
        });
//        $("#b3").click(function(){
//            window.location.href = "<?php echo base_url()?>main/settings";
//        });
//        $("#b4").click(function(){
//            window.location.href = "<?php echo base_url()?>main/settings_2";
//        });
        
        function add(){
            $("#f1_card").addClass("rotate");
            $("#f2_card").addClass("rotateX");
            $("#f3_card").addClass("rotate");
            $("#f4_card").addClass("rotateX");
            $("#f5_card").addClass("rotate");
            $("#f6_card").addClass("rotateX");
            $("#f7_card").addClass("rotate");
            setTimeout(function(){removeclass();},2000);
        }

        function removeclass(){
            $("#f1_card").removeClass("rotate");
            $("#f2_card").removeClass("rotateX");
            $("#f3_card").removeClass("rotate");
            $("#f4_card").removeClass("rotateX");
            $("#f5_card").removeClass("rotate");
            $("#f6_card").removeClass("rotateX");
            $("#f7_card").removeClass("rotate");
        }
        setInterval(function(){add();},4000);
    });
</script>
<?php $this->load->view("template/footer");?>