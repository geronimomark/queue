<?php $this->load->view("template/header_terminal");?>

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lightbox.css" media="screen"/> 
<script src="<?php echo base_url();?>assets/js/lightbox.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.js"></script>

<style>
    #gallery { float: left; width: 65%; min-height: 12em; }
    /*.gallery.custom-state-active { background: #eee; }*/
    /*.gallery div a { float: left; width: 96px; padding: 0.4em; margin: 0 0.4em 0.4em 0; text-align: center; }*/
    /*.gallery li h5 { margin: 0 0 0.4em; cursor: move; }
    .gallery li a { float: right; }
    .gallery li a.ui-icon-zoomin { float: left; }
    .gallery li img { width: 100%; cursor: move; }*/
    #trash { float: right; width: 32%; min-height: 18em; padding: 1%; }
    #trash h4 { line-height: 16px; margin: 0 0 0.4em; }
    #trash h4 .ui-icon { float: left; }
    /*#trash .gallery h5 { display: none; }*/
</style>

<header>
    <div class="container">
        <div class="four columns" id="logo">
            <img src="<?php echo base_url()?>assets/css/terminal/1logo.png" class="scale-with-grid" alt="Logo"></div>
        <!-- Your logo-->

        <div class="btn-responsive-menu"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
        <div class="twelve columns" id="top-nav">
            <ul>
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>main/index">Main Display</a></li>
                <li><a id="a" href="">Terminal Mode</a></li>
                <li><a href="<?php echo base_url()?>main/settings">Queue Settings</a></li>
                <li><a href="<?php echo base_url()?>main/settings_2">Terminal Settings</a></li>
                <li><a href="<?php echo base_url()?>upload">Ads Settings</a></li>
            </ul>
        </div>
    </div>
</header>
<!-- End Header-->
<section id="form_area_survey">
    <div id="shadow"></div>
    <article class="container">

        <?php if(isset($error)) echo "<strong>" . $error . "</strong>";?>

        <?php if(isset($upload_data)) echo "<strong>Image file " . $upload_data["file_name"] . " successfully saved.</strong><br />";?>

        <div class="ui-widget ui-helper-clearfix">
            <?php if(isset($images)):?>
                <h2>Click to view actual image</h2>
                <div id="gallery" class="image-row">
                    <div class="image-set">
                        <?php echo $images["images"];?>
                    </div>
                </div>

            <div id="trash" class="ui-widget-content ui-state-default">
                <h4 class="ui-widget-header"><span class="ui-icon ui-icon-trash">Trash</span>Trash</h4>
                <div class="image-row">
                    <div class="image-set">
                    <?php echo $images["trash"];?>
                    </div>
                </div>
            </div>
            <?php endif;?>
        </div> 

        <br />

        <h2>Add new image</h2>

        <?php echo form_open_multipart(base_url() . 'upload/do_upload');?>

        <input type="file" name="userfile" size="20" />

        <br /><br />

        <input type="submit" value="Upload" />

        </form>
                     
    </article>
    <div id="dialog" title="Confirm"></div>
    <div id="shadow_2"></div>
</section>
<!-- End Form Area -->

<script>
    $(function() {

        var $gallery = $( "#gallery" ) , $trash = $( "#trash" );

        $("#a").click(function(e) {
            e.preventDefault();
            newwindow = window.open("<?php echo base_url() ?>main/terminal_mode", "name", "scrollbars=0,height=400,width=330");
            if (window.focus)
                newwindow.focus();
        });

        $( "div a", $gallery ).draggable({
            cancel: "a.ui-icon", 
            revert: "invalid", 
            containment: "document",
            helper: "clone",
            cursor: "move"
        });

        $( "div div a", $trash ).draggable({
            cancel: "a.ui-icon", 
            revert: "invalid", 
            containment: "document",
            helper: "clone",
            cursor: "move"
        });

        $trash.droppable({
            accept: "#gallery div a",
            activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                deleteImage( ui.draggable );
            }
        });

        $gallery.droppable({
            accept: "#trash div div a",
            activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                recycleImage( ui.draggable );
            }
        });

        function recycleImage( $item ) {
            var $file = $item.context.id;
            $item.fadeOut(function(){
                $item
                    .find( "a.ui-icon-refresh" )
                    .remove()
                    .end()
                    .css( "width", "80px")
                    .find( "img" )
                    .css( "height", "80px" )
                    .css( "width", "80px")
                    .end()
                    .appendTo( $("#gallery div") )
                    .fadeIn();
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>upload/restore_image/" + $file
            });
        }

        function deleteImage( $item ) {
            var $file = $item.context.id;
            $item.fadeOut(function() {
                var $list = $( "div div", $trash );
                $item.find( "a.ui-icon-trash" ).remove();
                $item.appendTo( $list ).fadeIn(function() {
                    $item
                    .animate({ width: "50px" })
                    .find( "img" )
                    .animate({ height: "50px" })
                    .animate({ width: "50px" });
                });
            });
            $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>upload/delete_image/" + $file
            });

        }

    });
</script>
<?php $this->load->view("template/footer");?>