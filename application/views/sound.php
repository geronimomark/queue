<?php $this->load->view("template/header");?>
<script>
    $(function() {
        
        setTimeout(function(){msg1();},100);
        setTimeout(function(){num1();},900);
        setTimeout(function(){msg2();},1500);
        setTimeout(function(){num2();},3500);
    
        function msg1(){
            var _0 = new Audio('<?php  echo base_url();?>assets/audio/msg1.mp3'); 
            _0.play(); 
        }
        
        function num1(){
            var _1 = new Audio('<?php echo base_url();?>assets/audio/1.mp3'); 
            _1.play(); 
        }
        
        function msg2(){
            var _2 = new Audio('<?php echo base_url();?>assets/audio/msg2.mp3'); 
            _2.play(); 
        }
        
        function num2(){
            var _3 = new Audio('<?php echo base_url();?>assets/audio/2.mp3'); 
            _3.play();
        }
         
    });
    
</script>
<?php $this->load->view("template/footer");?>