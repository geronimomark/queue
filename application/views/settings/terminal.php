<?php $this->load->view("template/header_terminal"); ?>
<header>
    <div class="container">
        <div class="four columns" id="logo">
            <img src="<?php echo base_url() ?>assets/css/terminal/1logo.png" class="scale-with-grid" alt="Logo"></div>
        <!-- Your logo-->

        <div class="btn-responsive-menu"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
        <div class="twelve columns" id="top-nav">
            <ul>
                <li><a href="<?php echo base_url() ?>">Home</a></li>
                <li><a href="<?php echo base_url() ?>main/index">Main Display</a></li>
                <li><a id="a" href="">Terminal Mode</a></li>
                <li><a href="<?php echo base_url() ?>main/settings">Queue Settings</a></li>
                <li><a href="<?php echo base_url() ?>main/settings_2">Terminal Settings</a></li>
                <li><a href="<?php echo base_url()?>upload">Ads Settings</a></li>
            </ul>
        </div>
    </div>
</header>
<!-- End Header-->

<section id="form_area_survey">
    <div id="shadow"></div>
    <article class="container">

        <div class="five columns">
            <?php if ($this->session->userdata("terminal_id")): ?>
                <form id="custom" action="<?php echo base_url(); ?>main/unlink_terminal" method="POST">
                    <fieldset title="Terminal">
                        <legend>&nbsp;</legend>

                        <h3 class="question">Your Terminal Id: <?php echo $this->session->userdata("terminal_id"); ?></h3>

                    </fieldset>
                    <!-- End Step one -->
                    <input type="submit" class="finish" value="Change Terminal Settings" />
                </form>
            <?php else: ?>
                <form id="custom" action="<?php echo base_url(); ?>main/set_terminal" method="POST">
                    <fieldset title="Terminal">                    
                        <legend>&nbsp;</legend>
                        <!--<h3 class="question">1. How do you describe your overall satisfaction or dissatisfaction here?</h3>-->
                        <h3 class="question">Select Terminal ID:</h3>
                        <select name="terminal_id">
                            <?php foreach ($data as $value): ?>
                                <option value="<?php echo $value["terminal_id"]; ?>"><?php echo $value["terminal_id"]; ?></option>             
                            <?php endforeach; ?>
                        </select>
                        <br /><br />
                        <h3 class="question">Select Terminal Service:</h3>
                        <select name="terminal_service">
                            <option value="1">Western Union</option>
                            <option value="2">Bayad Center</option>
                            <option value="3">Ticketing</option>
                            <option value="4">Others</option>
                        </select>
                        <br /><br />
                        <h3 class="question">Optional:</h3>
                        <input type="checkbox" name="status" value="1"><strong>Check if terminal have separate queue</strong>
                    </fieldset>
                    <br />
                    <!-- End Step one -->
                    <input type="submit" class="finish" value="Save Terminal" />
                </form>
            <?php endif; ?>
        </div>
    </article>
    <div id="shadow_2"></div>
</section>
<!-- End Form Area -->
<script>
    $(function() {
        $("#a").click(function(e) {
            e.preventDefault();
            newwindow = window.open("<?php echo base_url() ?>main/terminal_mode", "name", "scrollbars=0,height=400,width=330");
            if (window.focus)
                newwindow.focus();
        });
    });

</script>
<?php $this->load->view("template/footer"); ?>