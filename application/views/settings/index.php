<?php $this->load->view("template/header_terminal");?>
<header>
    <div class="container">
        <div class="four columns" id="logo">
            <img src="<?php echo base_url()?>assets/css/terminal/1logo.png" class="scale-with-grid" alt="Logo"></div>
        <!-- Your logo-->

        <div class="btn-responsive-menu"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
        <div class="twelve columns" id="top-nav">
            <ul>
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>main/index">Main Display</a></li>
                <li><a id="a" href="">Terminal Mode</a></li>
                <li><a href="<?php echo base_url()?>main/settings">Queue Settings</a></li>
                <li><a href="<?php echo base_url()?>main/settings_2">Terminal Settings</a></li>
                <li><a href="<?php echo base_url()?>upload">Ads Settings</a></li>
            </ul>
        </div>
    </div>
</header>
<!-- End Header-->
<section id="form_area_survey">
    <div id="shadow"></div>
    <article class="container">

        <div class="five columns">
            <form id="custom" action="<?php echo base_url();?>main/set_config" method="POST">
                <fieldset title="Terminal">
                    <legend>&nbsp;</legend>
                    <!--<h3 class="question">1. How do you describe your overall satisfaction or dissatisfaction here?</h3>-->
                    <h3 class="question">Terminal Count:</h3>
                    <input type="text" id="terminal" name="terminal" class="number" value="<?php if ($config) echo $config[0]['config_terminals'];?>"/>
                    <h3 class="question">Queue Count:</h3>
                    <input type="text" id="queue" name="queue" class="number" value="<?php if ($config) echo $config[0]['config_queuecount'];?>"/>
                </fieldset>
                <!-- End Step one -->
                <input type="submit" id="save" class="finish" value="Save Configuration" />
            </form>
        </div>
    </article>
    <div id="dialog" title="Confirm"></div>
    <div id="shadow_2"></div>
</section>
<!-- End Form Area -->

<script>
    $(function() {
        var $save = $("#save"),
        $dialog = $("#dialog"),
        $number = $(".number");
        $number.keydown(function(event) {
            // Allow: backspace, delete, tab, escape, enter and .
//            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
            if ( $.inArray(event.keyCode,[46,8,9,27,13]) !== -1 ||
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        
        $("#a").click(function(e){
            e.preventDefault();
            newwindow=window.open("<?php echo base_url()?>main/terminal_mode","name","scrollbars=0,height=400,width=330");
            if (window.focus) newwindow.focus(); 
        });
        
        $save.click(function(e){
            e.preventDefault();
            if($("#terminal").val() > 0 && $("#queue").val() > 0){
                $dialog.html("<p>Save Configuration?</p>")
                    .dialog({
                        resizable: false,
                        modal: true,
                        buttons: {
                            "OK": function() {
                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    data: {terminal: $("#terminal").val(), queue:$("#queue").val()},
                                    url: "<?php echo base_url();?>main/set_config",
                                    success: function(msg){
                                        alert(msg);
                                    }
                                });
                                $( this ).dialog( "close" );
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });    
            }else{
                alert("Invalid Input!");
            }                   
        });
    });
    
</script>
<?php $this->load->view("template/footer");?>