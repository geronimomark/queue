<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

    class Db_query{
        
        private $CI;
        
        function __construct()
        {
            $this->CI =& get_instance();
        }
        
        function read_sp($proc, $param, $out)
        {
            $result = $this->sp_query($proc, $param, $out);
            $res = $result->result_array();
            $result->next_result();
            $result->free_result();
            return $res;
        }
        
        function write_sp($proc, $param, $out)
        {
            return $this->sp_query($proc, $param, $out);
        }
        
        function get_out($query)
        {
            $result = $this->CI->db->query($query);
            $res = $result->row_array();
            $result->next_result();
            $result->free_result();
            return $res;
        }
        
        private function sp_query($sp, $param, $out)
        {
            $args = $this->create_param(count($param), $out);
            $query = "CALL " . $sp . "(" . $args . ")";
            return $this->CI->db->query($query,$param);
        }

        private function create_param($count, $out)
        {
            $param = "";
            for ($x = 0; $x < $count; $x++) {
            if ($count === ($x + 1))
                $param .= "?";
            else
                $param .= "?,";
            }
            if ($out > 0) $param .= ",";
            for ($x = 0; $x < $out; $x++) {
            if (($out === ($x + 1)) || ($out === ($x + 1)))
                $param .= "@" . $x;
            else
                $param .= "@" . $x . ",";
            }
            return $param;
        }
    }
?>