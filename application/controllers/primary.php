<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Primary extends CI_Controller {

    public function __construct()
    {
        parent::__construct();   
    }
    
    public function index()
    {
        $this->load->view("primary/index");
    }
    
}

/* End of file primary.php */
/* Location: ./application/controllers/primary.php */