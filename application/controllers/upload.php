<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		
		$data["images"] = $this->gallery();
		$this->load->view('upload/upload_form', $data);
	}

	public function do_upload()
	{
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		// $config['max_size']	= '100';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
			$data["error"] = $this->upload->display_errors();
		else
			$data["upload_data"] = $this->upload->data();

		$data["images"] = $this->gallery();
		$this->load->view('upload/upload_form', $data);
	}

	private function gallery()
	{
		$dir = "./assets/uploads/";
		$file_display = array('jpg', 'jpeg', 'png', 'gif');

		$dir_contents = scandir($dir);
		$images = ""; 
	    foreach ($dir_contents as $file) 
	    {
			$file_type = strtolower(end(explode('.', $file)));

			if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true)
			{
				$images .= '<a id="' . $file . '" class="example-image-link" href="' . base_url() . 'assets/uploads/' . $file . '" 
				data-lightbox="example-set" ><img class="example-image" src="' . base_url() . 'assets/uploads/' . $file . '" 
				width="80" height="80"/></a>';
			}    
	    }

		$dir = "./assets/uploads_trash/";
		$dir_contents = scandir($dir);
		$trash = "";
	    foreach ($dir_contents as $file) 
	    {
			$file_type = strtolower(end(explode('.', $file)));

			if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true)
			{			
				$trash .= '<a id="' . $file . '" class="example-image-link" href="' . base_url() . 'assets/uploads_trash/' . $file . '" 
				data-lightbox="example-set" ><img class="example-image" src="' . base_url() . 'assets/uploads_trash/' . $file . '" 
				width="50" height="50"/></a>';	
			}    
	    }
	    $data["images"] = $images;
	    $data["trash"] = $trash;
	    return $data;
	}

	public function delete_image($image)
	{
		$file = './assets/uploads/' . $image;
		$newfile = './assets/uploads_trash/' . $image;

		if (!copy($file, $newfile)) {
		    echo "failed to copy $file...\n";
		}else{
			unlink($file);
		}
	}

	public function restore_image($image)
	{
		$file = './assets/uploads_trash/' . $image;
		$newfile = './assets/uploads/' . $image;

		if (!copy($file, $newfile)) {
		    echo "failed to copy $file...\n";
		}else{
			unlink($file);
		}
	}
    
}

/* End of file upload.php */
/* Location: ./application/controllers/upload.php */