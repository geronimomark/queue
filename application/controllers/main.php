<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();	
        $this->load->model('config_model');
        $data["config"] = $this->config_model->read("`rt_check_config`", array(), 0)[0]["CONFIG"];
        $data["terminal_id"] = $this->config_model->read("`rt_count_terminal`", array($this->input->ip_address()), 0)[0]["terminal_id"];
        $this->session->set_userdata($data);     
    }
    
    public function index()
    {
        $data["images"] = $this->gallery();
        $this->load->view('main/index', $data);  
    }
    
    public function set_config()
    {
        if ($this->input->post("terminal") > 0 && $this->input->post("queue") > 0)
        {
            if ($this->session->userdata("config") == 0)
                $type = "INSERT";
            else
                $type = "UPDATE";
            $data = array(
                "samplebranch",
                $this->input->post("terminal"),
                $this->input->post("queue"),
                $type,
                date("Y-m-d")
            );
            if($this->config_model->save("`rt_set_config`", $data, 0))
            {
                $this->session->set_userdata(array("config" => $this->config_model->read("`rt_check_config`", array(), 0)[0]["CONFIG"]));
                $this->session->unset_userdata(array("terminal" => "", "terminal_id" => "", "t_service" => "", "t_status" => ""));
                echo "Configuration successfully saved!";
//                redirect(base_url());
            }
            else
                echo "Error saving configuration.";
        }
        else
        {
            redirect(base_url());
        }
    }
    
    public function set_terminal()
    {
        if ($this->input->post("terminal_id") && $this->input->post("terminal_service"))
        {
            if ($this->config_model->read("`rt_check_terminal`", array($this->input->post("terminal_id")), 0)[0]["terminal_ip"] == "")
            {
                if ($this->config_model->save("`rt_update_terminal`", array($this->input->post("terminal_id"), $this->input->ip_address(), $this->input->post("terminal_service"), $this->input->post("status")), 0))
                {
                    $this->session->set_userdata(array("t_service" => $this->input->post("terminal_service"), "t_status" => $this->input->post("status"),"terminal_id" => $this->input->post("terminal_id"),"terminal" => $this->config_model->read("`rt_count_terminal`", array($this->input->ip_address()), 0)[0]["TERMINAL"]));
                    redirect(base_url());
                }
            }
        }
    }
    
    public function settings()
    {
        $data["config"] = $this->config_model->read("`rt_check_config`", array(), 0);
        $this->load->view('settings/index', $data);
    }
    
    public function settings_2()
    {
        if($this->session->userdata("config") == 0)
        {
            redirect(base_url() . "main/settings");
        }
        else
        {
            $data["data"] = $this->config_model->read("`rt_get_avail_terminals`", array(), 0);
            $this->load->view('settings/terminal', $data); 
        }
    }
    
    public function unlink_terminal()
    {
        if ($this->config_model->save("`rt_unlink_terminal`", array($this->session->userdata("terminal_id")), 0))
        {
            $this->session->unset_userdata(array("terminal_id" => "", "t_service" => "", "t_status" => ""));
            redirect(base_url() . "main/settings_2");            
        }
    }
    
    public function next_customer()
    {
        $this->config_model->save("`rt_save_log`", array($this->session->userdata("terminal_id"), date("Y-m-d"), $this->session->userdata("t_service"), $this->session->userdata("t_status")), 0);
        $data = $this->config_model->read("`rt_get_customer`", array(date("Y-m-d"), $this->session->userdata("terminal_id")), 0);
        $this->config_model->save("`rt_add_display`", array($this->session->userdata("terminal_id"), $data[0]["cq_cust_number"], date("Y-m-d")), 0);
        echo $data[0]["cq_cust_number"] . "_" . $data[0]["cq_cust_total"];
    }
    
    public function terminal_mode()
    {
        if($this->session->userdata("config") == 0)
        {
            redirect(base_url() . "main/settings");
        } 
        elseif ($this->session->userdata("terminal_id") == 0)
        {
            redirect(base_url() . "main/settings_2");
        }
        else
        {
            if(! $this->session->userdata("t_service"))
            {
                $data["service"] = $this->config_model->read("`rt_get_service`", array($this->session->userdata("terminal_id")), 0);
                $this->session->set_userdata(array("t_service" => $data["service"][0]["terminal_service"], "t_status" => $data["service"][0]["terminal_status"]));
            }
            $data["data"] = $this->config_model->read("`rt_get_customer`", array(date("Y-m-d"), $this->session->userdata("terminal_id")), 0);
            $this->load->view('terminal/index', $data);           
        }
    }
    
    public function generate_table()
    {
        // $data = $this->config_model->read("`rt_generate_table`", array(date("Y-m-d")), 0);
        $data = $this->config_model->read("`rt_generate_table_new`", array(date("Y-m-d")), 0);
        $count = count($data);
        $html = "<span class='countdown_row countdown_show2'>";
        if($count == 0)
        {
            $html .= "<br /><br /><br /><br /><br /><br /><span class='sixteen columns countdown_section'><span class='count_titles_5'><img src='" . base_url() . "assets/images/ussc.png' width='500' height='200'/>"; 
        }
        elseif($count == 1)
        {
            $cols = 1;
            $title1 = "<span class='count_titles'>Counter</span><hr />";
            $title2 = "<span class='count_titles'>Now Serving</span><hr />";
            $notitle = "<span class='count_titles'>&nbsp;</span><hr>";
            $column = "four columns";
            $white = "countdown_amount1";
            $red = "countdown_amount";
        }
        elseif($count >= 2 && $count <= 4)
        {
            $cols = 2;
            $title1 = "<span class='count_titles_2'>Counter</span><hr />";
            $title2 = "<span class='count_titles_2'>Now Serving</span><hr />";
            $notitle = "<span class='count_titles_2'>&nbsp;</span><hr>";
            $column = "zero columns";
            $white = "countdown_amount3";
            $red = "countdown_amount2";
        }
        else
        {
            $cols = 3;
            $title1 = "<span class='count_titles_3'>Counter</span><hr />";
            $title2 = "<span class='count_titles_3'>Now Serving</span><hr />";
            $notitle = "<span class='count_titles_3'>&nbsp;</span><hr>";
            $column = "three columns";
            $white = "countdown_amount5";
            $red = "countdown_amount4";
        }
        
        $x = 1;
        $z = false;
        foreach ($data as $value) 
        {
            $span1 = "<span class='" . $column . " countdown_section'>";
            $span2 = "<span class='" . $column . " countdown_section'>";
            if($cols >= $x)
            {
                $span1 .= $title1;
                $span2 .= $title2;
            }
            else
            {
                $span1 .= $notitle;
                $span2 .= $notitle;
            }
            $span1 .= "<span class='" . $white . "'>" . $value["terminal_id"] . "</span>";
            $span2 .= "<span id='" . $value["terminal_id"] . "' class='" . $red . "'>" . ($value["cq_cust_number"] + 0) . "</span>";                                
  
            if($this->get_service($value["terminal_id"]) == 1)
                $span1 .= "<img src='" . base_url() . "assets/images/western_union.jpg' />";
            elseif($this->get_service($value["terminal_id"]) == 2)
                $span1 .= "<img src='" . base_url() . "assets/images/1.jpg' />";
            elseif($this->get_service($value["terminal_id"]) == 3)
                $span1 .= "<img width='200' height='90' src='" . base_url() . "assets/images/tickets.jpg' />";
            else
                $span1 .= "<img width='200' height='90' src='" . base_url() . "assets/images/ussc.png' />";
    
            $span2 .= "<img width='200' height='90' src='" . base_url() . "assets/images/ussc.png' />";
            
            if(($value["terminal_id"] > 99 || $value["cq_cust_number"] > 99) && $z === false)
                $z = true;
                
            $html .= $span1 . "</span>" . $span2 . "</span>";
            $x++;
        }
        
        if($count == 1)
        {
            $html = str_replace ("<img", "<br/><img", $html);
        }
        
        if($z && $count == 2)
        {
            $html = str_replace ("countdown_amount2", "countdown_amount22", $html);
            $html = str_replace ("countdown_amount3", "countdown_amount33", $html);
        }
        elseif ($z && $count == 3)
        {
            $html = str_replace ("countdown_amount4", "countdown_amount44", $html);
            $html = str_replace ("countdown_amount5", "countdown_amount55", $html);
        }

        if($cols == 3)
        {
            
            $html = str_replace (".jpg'", ".jpg' width='140' height='70'", $html);
            $html = str_replace ("200", "140", $html);
            $html = str_replace ("90", "70", $html);
        }
        $this->session->set_userdata(array("table" => $data));
        echo $html .= "</span>";
    }
    
    private function get_service($terminal)
    {
        $data = $this->config_model->read("`rt_get_service`", array($terminal), 0);
        return $data[0]["terminal_service"];
    }
    
    public function big_display()
    {
        $data = $this->config_model->read("`rt_get_display`", array(date("Y-m-d")), 0);
        if(count($data) > 0)
        {
            if($this->session->userdata("last") == $data[0]["terminal_id"] . "_" . $data[0]["cust_number"])
            {
                echo "error";
            }
            else
            {
                $this->config_model->save("`rt_remove_display`", array($data[0]["terminal_id"], date("Y-m-d")), 0);
                echo $data[0]["terminal_id"] . "_" . $data[0]["cust_number"];        
            }
            $this->session->set_userdata(array("last" => $data[0]["terminal_id"] . "_" . $data[0]["cust_number"]));
        }
        else
        {
            echo "error";            
        }
    }
      
    public function recall($number = FALSE)
    {
        $this->session->set_userdata(array("last" => "0_0"));
        if($number != FALSE)
           $this->config_model->save("`rt_recall`", array($this->session->userdata("terminal_id"), $number, date("Y-m-d")), 0);
    }

    private function gallery()
    {
        $dir = "./assets/uploads/";
        $file_display = array('jpg', 'jpeg', 'png', 'gif');

        $dir_contents = scandir($dir);
        $images = ""; 
        $ctr = 0;
        foreach ($dir_contents as $file) 
        {
            $file_type = strtolower(end(explode('.', $file)));

            if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true)
            {
                $ctr++;
                $images .= "<li><h2>" . $file . "</h2><img src='" . base_url() . "assets/uploads/" . $file . "' alt=''></li>";
            }    
        }
        if($ctr > 0)
        {
            $image["styles"] = $this->gallery_styles($ctr);
            $image["images"] = $images;
            return $image;
        }
        else
        {
            return false;
        }

    }

    private function gallery_styles($ctr)
    {
        $secs = $ctr * 2 * 5;
        $width = $ctr * 100;
        $pane = 100 / $ctr;
        $keyframe = $this->generate_keyframe($ctr);
        $styles = "<style>
                    .carousel{
                        overflow:hidden;
                        margin:0 auto;
                        width:700px;
                        height:300px; 
                    }
                    .panes{
                        list-style:none;
                        position:relative;
                        width:" . $width . "%;
                        overflow:hidden; 

                        -moz-animation:carousel ".$secs."s infinite;
                        -webkit-animation:carousel ".$secs."s infinite;
                        animation:carousel ".$secs."s infinite;
                    }
                    .panes > li{
                        position:relative;
                        float:left;
                        width:" . $pane . "%;
                    }
                    .carousel img{
                        display:block;
                        width:700px;
                        height:300px;
                    }
                    .carousel h2{
                        font-size:1em;
                        padding:0.5em;
                        position:absolute;
                        right:10px;
                        bottom:10px;
                        left:10px;
                        text-align:right;
                        color:#fff;
                        background-color:rgba(0,0,0,0.75);
                    }
                    " . $keyframe . "
                    </style>";
        return $styles;
    }

    private function generate_keyframe($ctr)
    {
        $percentage = 100 / ($ctr * 2);
        $temp = 0;
        $temp2 = 0;
        $temp3 = 0;
        $temp4 = 0;
        $kf = "@keyframes carousel{";
        while ($temp <= 50) {
            $temp3 = $temp2 * 100;
            $temp4 = ($temp3 == 0) ? $temp3 : ($temp3 * -1) . "%";
            $kf .= $temp . "% { left: " . $temp4 . ";}";
            $temp += $percentage;
            $temp2 ++;
        }

        $temp2 --;

        while ($temp <= 100) {
            $temp2 --;
            $temp3 = $temp2 * 100;
            $temp4 = ($temp3 == 0) ? $temp3 : ($temp3 * -1) . "%";
            $kf .= $temp . "% { left: " . $temp4 . ";}";
            $temp += $percentage;
            
        }
        $kf .= "}";
        return $kf;
    }
}

        // $keyframe = "@keyframes carousel{
        //                 0%    { left:0; }
        //                 11%   { left:0; }
        //                 12.5% { left:-100%; }
        //                 23.5% { left:-100%; }
        //                 25%   { left:-200%; }
        //                 36%   { left:-200%; }
        //                 37.5% { left:-300%; }
        //                 48.5% { left:-300%; }
        //                 50%   { left:-400%; }
        //                 61%   { left:-400%; }
        //                 62.5% { left:-300%; }
        //                 73.5% { left:-300%; }
        //                 75%   { left:-200%; }
        //                 86%   { left:-200%; }
        //                 87.5% { left:-100%; }
        //                 98.5% { left:-100%; }
        //                 100%  { left:0; }
        //             }";

/* End of file main.php */
/* Location: ./application/controllers/main.php */