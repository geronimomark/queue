<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Config_model extends CI_Model{
        
    public function __construct()
    {
        parent::__construct();	
        $this->load->library('db_query');       
    }
    
    public function save($proc, $param, $out)
    {
        return $this->db_query->write_sp($proc, $param, $out);
    }
    
    public function read($proc, $param, $out)
    {
        return $this->db_query->read_sp($proc, $param, $out);
    }
    
//    public function check_config()
//    {
//        return $this->db_query->read_sp("`rt_check_config`", array(), 0);
//    }
//    
//    public function count_terminal($ip = FALSE)
//    {
//        return $this->db_query->read_sp("`rt_count_terminal`", array($ip), 0);
//    }
    
//    public function check_terminal($id)
//    {
//        return $this->db_query->read_sp("`rt_check_terminal`", array($id), 0);
//    }
    
//    public function update_terminal($id, $ip)
//    {
//        return $this->db_query->write_sp("`rt_update_terminal`", array($id,$ip), 0);
//    }
    
//    public function get_avail_terminals()
//    {
//        return $this->db_query->read_sp("`rt_get_avail_terminals`", array(), 0);
//    }
    
//    public function unlink_terminal($id)
//    {
//        return $this->db_query->write_sp("`rt_unlink_terminal`", array($id), 0);
//    }
    
//    public function set_config($data)
//    {
//        return $this->db_query->write_sp("`rt_set_config`", $data, 0);
//    }
    
//    public function get_customer()
//    {
//        return $this->db_query->read_sp("`rt_get_customer`", array(date("Y-m-d"), $this->session->userdata("terminal_id")), 0);
//    }
    
//    public function save_log($data)
//    {
//        return $this->db_query->write_sp("`rt_save_log`", $data, 0);
//    }
    
//    public function generate_table()
//    {
//        return $this->db_query->read_sp("`rt_generate_table`", array(date("Y-m-d")), 0);
//    }
    
//    public function add_display($data)
//    {
//        return $this->db_query->write_sp("`rt_add_display`", $data, 0);
//    }
    
//    public function get_display()
//    {
//        return $this->db_query->read_sp("`rt_get_display`", array(date("Y-m-d")), 0);
//    }
    
//    public function remove_display($data)
//    {
//        return $this->db_query->write_sp("`rt_remove_display`", $data, 0);
//    }
    
}