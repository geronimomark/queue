-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2013 at 02:49 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `customer_queueing`
--
CREATE DATABASE IF NOT EXISTS `customer_queueing` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `customer_queueing`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_add_display`(IN `_terminal` INT, IN `_customer` INT, IN `_date` VARCHAR(10))
    NO SQL
BEGIN
	IF EXISTS(SELECT `terminal_id` FROM `cq_logs2` WHERE `terminal_id` = _terminal AND `log_date` LIKE CONCAT(_date, '%')) THEN
		UPDATE `cq_logs2`
		SET `cust_number` = _customer,
		`log_date` = NOW(),
		`status` = 1
		WHERE `terminal_id` = _terminal;
	ELSE
		INSERT INTO `cq_logs2`()
		VALUES(
        	_terminal,
            NOW(),
            _customer,
            1
        );
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_check_config`()
    NO SQL
BEGIN
	SELECT count(*) as "CONFIG", `config_terminals`, `config_queuecount`
	FROM `cq_configuration`;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_check_terminal`(IN `_id` INT)
    NO SQL
BEGIN
	SELECT *
	FROM `cq_terminal`
	WHERE `terminal_id` = _id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_count_terminal`(IN `_ip` VARCHAR(255))
    NO SQL
BEGIN
	SELECT COUNT(*) AS "TERMINAL", `terminal_id`
	FROM `cq_terminal`
	WHERE `terminal_ip` = _ip;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_generate_table`(IN `_date` VARCHAR(30))
    NO SQL
BEGIN
	SELECT `terminal_id`, `cq_cust_number`
	FROM `cq_logs`
	WHERE `cq_logdate` LIKE CONCAT(_date, '%')
	ORDER BY `terminal_id` ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_generate_terminals`(IN `_terminal` INT, IN `_date` VARCHAR(10))
    NO SQL
BEGIN
	TRUNCATE `cq_terminal`;
	DELETE FROM `cq_logs` WHERE `cq_logdate` LIKE CONCAT(_date, '%');
	DELETE FROM `cq_logs2` WHERE `log_date` LIKE CONCAT(_date, '%');
	SET @x = 1;
	WHILE @x <= _terminal DO		
		INSERT INTO `cq_terminal`()
		VALUES(@x , "", 0, 0);
		SET @x = @x + 1;
	END WHILE;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_get_avail_terminals`()
    NO SQL
BEGIN
	SELECT `terminal_id`
	FROM `cq_terminal`
	WHERE `terminal_ip` = "";
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_get_customer`(IN `_date` VARCHAR(30), IN `_terminal` INT)
    NO SQL
BEGIN
	SELECT `cq_cust_number`, `cq_cust_total`
	FROM `cq_logs`
	WHERE `terminal_id` = _terminal 
	AND `cq_logdate` LIKE CONCAT(_date, '%');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_get_display`(IN `_date` VARCHAR(10))
    NO SQL
BEGIN
	SELECT `terminal_id`, `cust_number`
    FROM `cq_logs2`
    WHERE `status` = 1
	AND `log_date` LIKE CONCAT(_date, '%')
    ORDER BY `log_date`
	LIMIT 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_get_service`(IN `_terminal` INT)
    NO SQL
BEGIN
	SELECT `terminal_service`, `terminal_status`
	FROM `cq_terminal` 
	WHERE `terminal_id` = _terminal;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_recall`(IN `_terminal` INT, IN `_number` INT, IN `_date` VARCHAR(10))
    NO SQL
BEGIN 
	UPDATE `cq_logs2`
	SET `status` = 1
	WHERE `terminal_id` = _terminal
	AND `cust_number` = _number 
	AND `log_date` LIKE CONCAT(_date, '%');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_remove_display`(IN `_terminal` INT, IN `_date` VARCHAR(10))
    NO SQL
BEGIN
	UPDATE `cq_logs2`
	SET `status` = 0
	WHERE `terminal_id` = _terminal
	AND `log_date` LIKE CONCAT(_date, '%');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_save_log`(IN `_terminal` INT, IN `_date` VARCHAR(30), IN `_service` INT, IN `_status` INT)
    NO SQL
BEGIN
	SET @max = (SELECT `config_queuecount` FROM `cq_configuration` LIMIT 1);
	IF _status > 0 THEN
		SET @active = (SELECT `cq_cust_number` FROM `cq_logs` INNER JOIN `cq_terminal` ON `cq_logs`.`terminal_id` = `cq_terminal`.`terminal_id` AND `terminal_service` = _service AND `terminal_status` = _status AND `cq_logdate` LIKE CONCAT(_date, '%') ORDER BY `cq_logdate` DESC LIMIT 1);
	ELSE
		SET @active = (SELECT `cq_cust_number` FROM `cq_logs` INNER JOIN `cq_terminal` ON `cq_logs`.`terminal_id` = `cq_terminal`.`terminal_id` AND `terminal_status` = 0 AND `cq_logdate` LIKE CONCAT(_date, '%') ORDER BY `cq_logdate` DESC LIMIT 1);
	END IF;

	IF @active < @max THEN
		SET @active = @active + 1;
	ELSE
		SET @active = 1;
	END IF;

	SET @z = false;

	WHILE @z = false DO
        IF _status > 0 THEN
			IF EXISTS(SELECT `cq_cust_number` FROM `cq_logs` INNER JOIN `cq_terminal` ON `cq_logs`.`terminal_id` = `cq_terminal`.`terminal_id` AND `terminal_service` = _service AND `terminal_status` = _status AND `cq_logdate` LIKE CONCAT(_date, '%') AND `cq_cust_number` = @active ORDER BY `cq_logdate` DESC LIMIT 1) THEN
                IF @active < @max THEN
                    SET @active = @active + 1;
                ELSE
                    SET @active = 1;
                END IF;
            ELSE
                SET @z = true;
            END IF;
		ELSE
            IF EXISTS(SELECT `cq_cust_number` FROM `cq_logs` INNER JOIN `cq_terminal` ON `cq_logs`.`terminal_id` = `cq_terminal`.`terminal_id` AND `terminal_status` = 0 AND `cq_logdate` LIKE CONCAT(_date, '%') AND `cq_cust_number` = @active ORDER BY `cq_logdate` DESC LIMIT 1) THEN
                IF @active < @max THEN
                    SET @active = @active + 1;
                ELSE
                    SET @active = 1;
                END IF;
            ELSE
                SET @z = true;
            END IF;
		END IF;
	END WHILE;

	IF EXISTS(SELECT * FROM `cq_logs` WHERE `terminal_id` = _terminal AND `cq_logdate` LIKE CONCAT(_date, '%')) THEN
		SET @total = (SELECT `cq_cust_total` FROM `cq_logs` WHERE `terminal_id` = _terminal AND `cq_logdate` LIKE CONCAT(_date, '%')) + 1;			
		UPDATE `cq_logs`
		SET `cq_cust_number` = @active,
		`cq_cust_total` = @total,
		`cq_logdate` = NOW()
		WHERE `terminal_id` = _terminal AND `cq_logdate` LIKE CONCAT(_date, '%');
	ELSE
		IF EXISTS(SELECT `terminal_id` FROM `cq_terminal` WHERE `terminal_id` = _terminal) THEN
			INSERT INTO `cq_logs`(
                `terminal_id`,
                `cq_logdate`,
                `cq_cust_number`,
                `cq_cust_total`
            )
            VALUES(
                _terminal,
                NOW(),
                @active,
                1
            );
		END IF;		
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_set_config`(IN `_branch` VARCHAR(255), IN `_terminal` INT, IN `_queue` INT, IN `_type` VARCHAR(10), IN `_date` VARCHAR(10))
    NO SQL
BEGIN
	IF _type = "INSERT" THEN
		INSERT INTO `cq_configuration`(
            `config_branchid`,
            `config_terminals`,
            `config_queuecount`
        )
		VALUES(
        	_branch,
            _terminal,
            _queue
        );
	ELSE
		UPDATE `cq_configuration`
		SET `config_branchid` = _branch,
		`config_terminals` = _terminal,
		`config_queuecount` = _queue
		WHERE `config_id` = 1;
	END IF;
	CALL `rt_generate_terminals`(_terminal, _date);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_unlink_terminal`(IN `_id` INT)
    NO SQL
BEGIN
	UPDATE `cq_terminal`
	SET `terminal_ip` = "",
	`terminal_service` = 0,
	`terminal_status` = 0
	WHERE `terminal_id` = _id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_update_terminal`(IN `_id` INT, IN `_ip` VARCHAR(255), IN `_service` INT, IN `_status` INT)
    NO SQL
BEGIN
	IF NOT EXISTS(SELECT * FROM `cq_terminal` WHERE `terminal_ip` = _ip AND `terminal_id` <> _id) THEN
        UPDATE `cq_terminal`
        SET `terminal_ip` = _ip,
		`terminal_service` = _service,
		`terminal_status` = _status
        WHERE `terminal_id` = _id;
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cq_configuration`
--

CREATE TABLE IF NOT EXISTS `cq_configuration` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_branchid` varchar(255) NOT NULL,
  `config_terminals` int(11) NOT NULL DEFAULT '1',
  `config_queuecount` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cq_configuration`
--

-- --------------------------------------------------------

--
-- Table structure for table `cq_logs`
--

CREATE TABLE IF NOT EXISTS `cq_logs` (
  `cq_logid` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_id` int(11) NOT NULL,
  `cq_logdate` datetime NOT NULL,
  `cq_cust_number` int(11) NOT NULL,
  `cq_cust_total` int(11) NOT NULL,
  PRIMARY KEY (`cq_logid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cq_logs`
--

-- --------------------------------------------------------

--
-- Table structure for table `cq_logs2`
--

CREATE TABLE IF NOT EXISTS `cq_logs2` (
  `terminal_id` int(11) NOT NULL,
  `log_date` datetime NOT NULL,
  `cust_number` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cq_logs2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cq_service`
--

CREATE TABLE IF NOT EXISTS `cq_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cq_terminal`
--

CREATE TABLE IF NOT EXISTS `cq_terminal` (
  `terminal_id` int(11) NOT NULL,
  `terminal_ip` varchar(255) NOT NULL,
  `terminal_service` int(11) NOT NULL,
  `terminal_status` int(11) NOT NULL,
  PRIMARY KEY (`terminal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cq_terminal`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
